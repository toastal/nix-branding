{ pkgs ? import <nixpkgs> {}
, colors, primary-color, background-color
, name, twitterHandle
, mkLogo, mkThumbnail
}: {

  colorModules = import ./colorFiles.nix { inherit colors pkgs; };
  logo         = import ./logo.nix { inherit colors mkLogo mkThumbnail pkgs; };
  icons        = import ./icons.nix { inherit colors primary-color background-color name pkgs; };
  socialHTML   = import ./social.nix { inherit pkgs name twitterHandle; };
  format       = import ./format.nix { inherit pkgs; };
  inherit (import ./util.nix { inherit pkgs; }) genFile;

}
