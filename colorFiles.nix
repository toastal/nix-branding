{ colors ? { white = { r = 255; g = 255; b = 255; }; }, pkgs ? import <nixpkgs> {} }: with pkgs; with lib; with builtins;
let

  format = import ./format.nix { inherit pkgs; };
  inherit (import ./util.nix { inherit pkgs; }) genFile;

in rec {



  typescript = genFile "//" "Colors.ts" ''
    export type Color
      = ${builtins.concatStringsSep "\n  | " (mapAttrsToList (_: value: ''"${format.rgbToHex value}"'') colors)};

    ${builtins.concatStringsSep "\n" (mapAttrsToList (name: value:
      ''export const ${format.toPascalCase name}: Color = "${format.rgbToHex value}";'') colors)}
  '';


  elm = genFile "--" "Colors.elm" ''
    module Colors exposing (..)


    type ColorHex = ColorHex String
    unColorHex (ColorHex s) = s


    type alias ColorRGB = { r : Int, g : Int, b : Int }


    type Color
      = ${builtins.concatStringsSep "\n  | " (mapAttrsToList (name: _: format.toPascalCase name) colors)}


    getColorHex : Color -> ColorHex
    getColorHex c = case c of
      ${builtins.concatStringsSep "\n  " (mapAttrsToList (name: value:
      ''${format.toPascalCase name} -> ColorHex "${format.rgbToHex value}"'') colors)}


    getColorRGB : Color -> ColorRGB
    getColorRGB c = case c of
      ${builtins.concatStringsSep "\n  " (mapAttrsToList (name: {r,g,b}:
      ''${format.toPascalCase name} -> { r = ${toString r}, g = ${toString g}, b = ${toString b} }'') colors)}
  '';


  haskell = genFile "--" "Colors.hs" ''
    module Colors where


    data Color
      = ${builtins.concatStringsSep "\n  | " (mapAttrsToList (name: _: format.toPascalCase name) colors)}
      deriving (Show, Read, Eq, Ord, Bounded, Enum)


    newtype ColorHex = ColorHex { unColorHex :: String }
      deriving (Show, Read, Eq, Ord, Bounded, Enum)

    data ColorRGB = ColorRGB { r :: Int, g :: Int, b :: Int }
      deriving (Show, Read, Eq, Ord, Bounded, Enum)


    getColorHex :: Color -> ColorHex
    getColorHex c = case c of
      ${builtins.concatStringsSep "\n  " (mapAttrsToList (name: value:
      ''${format.toPascalCase name} -> ColorHex "${format.rgbToHex value}"'') colors)}


    getColorRGB :: Color -> ColorRGB
    getColorRGB c = case c of
      ${builtins.concatStringsSep "\n  " (mapAttrsToList (name: {r,g,b}:
      ''${format.toPascalCase name} -> ColorRGB { r = ${toString r}, g = ${toString g}, b = ${toString b} }'') colors)}
  '';


  sass = genFile "//" "Colors.sass" (concatStrings (mapAttrsToList (name: value: ''
    ''$${name}: ${format.toCSSString value}
  '') colors));

  css = genFile "//" "Colors.css" ''
  :root{
  ${concatStringsSep "\n" (mapAttrsToList (name: value:
  "   --${name}: ${format.toCSSString value};")
  colors)}
  }
  '';

}
